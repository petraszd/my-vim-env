if has('python3')
  silent! python3 1
endif

" Look
" ----

set nocompatible
set textwidth=119
syntax on
set number
set nohlsearch
set t_Co=256
set background=dark
set hlsearch
"colorscheme hybrid  " lucius
colorscheme lucius
set colorcolumn=120
hi ColorColumn ctermbg=235 guibg=#262626

au VimEnter * set vb t_vb=
set guioptions-=m
set guioptions-=T

set modelines=10
set fileencoding=utf8

set backspace=indent,eol,start
set directory=~/.vim/tmp

if has("gui_macvim")
  set guioptions=
  set guifont=Monaco:h13
endif

set clipboard=unnamed

" Fuck folding
set foldmethod=indent
set nofoldenable

" Behaviour
" ---------

set exrc
set secure

" remapping
nmap <space>h <C-w>h
nmap <space>j <C-w>j
nmap <space>k <C-w>k
nmap <space>l <C-w>l
nmap <space>w <C-w>w
nmap <space>v <C-w>v
nmap <space>c <C-w>c
nmap <space>o <C-w>o

nmap <space>s :wa<CR>

nmap <space>e :Ex<CR>
nmap <space>b :B<CR>

inoremap kk <C-p>
inoremap jj <C-n>

" indentation
set autoindent
set smartindent
filetype on
filetype plugin on
filetype plugin indent on

" Removes trailing spaces
function! RemoveTrailingSpaces()
  let _s=@/
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  let @/=_s
  call cursor(l, c)
endfunction
au BufWritePre * call RemoveTrailingSpaces()

" tabs
set tabstop=2 " tab size in spaces
set expandtab " expand tabs to spaces
set shiftwidth=2 " number of spaces used with (auto) indent,

" grep
"set grepprg=grep -n $* /dev/null
set grepprg=grep\ --exclude-dir=env\ --exclude='*.pyc'\ --exclude-dir=node_modules\ --exclude-dir='.git'\ --exclude-dir='.mypy_cache'\ --exclude-dir='build'\ --exclude-dir='packed'\ --exclude='*.min.js'\ --exclude='*.map'\ --exclude='*.po'\ --exclude='package-lock.json'\ --exclude=tags\ --exclude='*.built.js'\ -n\ $*\ /dev/null

function! GrepInCode(grep_str)
  execute "grep " . shellescape(a:grep_str) . " . -r --exclude-dir=env --exclude-dir=node_modules --exclude-dir=\".git\" --exclude-dir=\"./flask/app/assets\" --include=\"*.py\" --include=\"*.tsx\"  --include=\"*.ts\" --include=\"*.jsx\" --include=\"*.js\" --include=\"*.css\" --include=\"*.less\" --include=\"*.scss\" --include=\"*.html\" --include=\"*.graphql\""
endfunction

function! GrepInCodeUsingSearch()
  let l:search_str = @/
  let l:n = strlen(l:search_str)

  if strpart(l:search_str, 0, 2) == "\\<" && strpart(l:search_str, l:n - 2) == "\\>"
    let l:search_str = strpart(l:search_str, 2, l:n - 4)
  endif

  call GrepInCode(l:search_str)
endfunction

map <F5> <ESC>:call GrepInCodeUsingSearch()<CR>

" netrw
let g:netrw_liststyle = 3
let g:netrw_list_hide = '.*\.pyc$'

" Plugins
" -------

" NERDTree
command Ex execute "e " . expand("%:p:h")
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeIgnore = ['\.pyc$']

" NERDCommenter
let g:mapleader = ','

" Buffet
command B Bufferlistsw

" PyFlakes
highlight SpellBad term=reverse ctermbg=1

" CtrlP
let g:ctrlp_custom_ignore = {
      \ 'dir':  '\v[\/]\.(git|hg|svn)$',
      \ 'file': '\v\.(pyc)$',
      \ }
let g:ctrlp_map = '<space>p'

" Ale
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_echo_msg_format = '[%linter% | %severity% | %code%] %s'
let g:ale_lint_delay = 500

let g:ale_linter_aliases = {'typescriptreact': 'typescript'}
let g:ale_linters = {
      \'c': ['clang'],
      \'python': ['flake8', 'pylint'],
      \'typescript': ['tslint', 'tsserver', 'typecheck'],
      \'cs': ['pz_omnisharp'],
      \}
let g:ale_fixers = {
      \'typescript': [],
      \}
let g:ale_python_pylint_options = '--disable=missing-docstring,invalid-name,too-few-public-methods,blacklisted-name,redefined-outer-name,import-error,unused-argument,no-self-use,no-name-in-module,no-else-return,c-extension-no-member --load-plugins pylint_django --max-line-length=120'
let g:ale_python_flake8_options = '--ignore D100,D101,D102,D103,D105,T001,T003,T101 --max-line-length 120'

" UltiSnips
let g:UltiSnipsExpandTrigger="<c-j>"

" Airline
let g:airline_extensions = []

" YCM
let g:ycm_server_python_interpreter = 'python3'
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_min_num_identifier_candidate_chars = 1
let g:ycm_auto_trigger = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_show_diagnostics_ui = 0

" NERDCommenter
let g:NERDCustomDelimiters = {
      \ 'javascript': {'left': '// ', 'leftAlt': '/*', 'rightAlt': '*/'}
      \ }

" python-syntax
let g:python_highlight_all = 1

" OmniSharp-vim
let g:OmniSharp_highlight_types = 1

" FileType
" --------

au FileType python,c,cpp,java,cg setlocal shiftwidth=4 tabstop=4 et

" Javascript
au FileType javascript setlocal nocindent et

" cpp
au FileType cpp setlocal nocp cindent et
au FileType c setlocal cindent et

" glsl
au BufRead,BufNewFile *.glsl set filetype=glsl

" Make
autocmd FileType make setlocal noexpandtab
autocmd FileType make setlocal shiftwidth=8 shiftwidth=8 tabstop=8

" FZF

nmap <space>p :FZF<CR>

" ctags
" -----

function! GetCtagsFile()
  let l:ctags_file = findfile('tags', '.;')
  if l:ctags_file == ""
    let l:ctags_file = simplify(getcwd() . '/tags')
  endif
  return fnameescape(l:ctags_file)
endfunction

function! GetCtagsDir()
  return fnamemodify(GetCtagsFile(), ":h")
endfunction

function! RegenerateCtags()
  execute "silent !ctags -R --exclude=\*.pyc --exclude=\*.min.\* --exclude=.git --exclude=node_modules --exclude=env -f " . GetCtagsFile() . " " . GetCtagsDir() . " &"
endfunction

execute "set tags+=" . GetCtagsFile()

map <F3> <ESC>:call RegenerateCtags()<CR>
